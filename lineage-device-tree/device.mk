#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.crda.sh \
    init.qcom.crashdata.sh \
    init.qcom.sensors.sh \
    init.qti.chg_policy.sh \
    setup_mainmic2headphone.sh \
    qca6234-service.sh \
    init.qcom.coex.sh \
    init.class_main.sh \
    init.qcom.post_boot.sh \
    init.qti.ims.sh \
    init.qcom.usb.sh \
    capture.sh \
    init.mdm.sh \
    teardown_loopback.sh \
    capture_headset.sh \
    init.qcom.early_boot.sh \
    playback.sh \
    setup_topmic2headphone.sh \
    setup_headsetmic2headphone.sh \
    playback_headset.sh \
    init.qcom.efs.sync.sh \
    init.qcom.sh \
    init.qcom.sdio.sh \
    init.qti.fm.sh \
    init.qcom.class_core.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.qcom.usb.rc \
    init.msm.usb.configfs.rc \
    init.target.rc \
    init.qcom.factory.rc \
    init.qcom.rc \
    init.remosaic.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 27

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/xiaomi/beryllium/beryllium-vendor.mk)
