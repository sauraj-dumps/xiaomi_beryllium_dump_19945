#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:37442894:5f5c034b684711482b12bba37342d9d75c7bfdca; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:22787402:3a4a43007b5051c7b755f946a9d93d047dad4f5c \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:37442894:5f5c034b684711482b12bba37342d9d75c7bfdca && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
